console.log('Start API');

const url = 'http://localhost:3000/api/mdb_product';

const headers = new Headers();
headers.append('Content-Type', 'application/json');

const requestOptions = {
    method: 'GET',
    headers: headers,
    mode: 'cors'
};

fetch(url, requestOptions)
    .then(response => {
        console.log('res', response);
        return response.json();
    })
    .then(body => {
        console.log('body', body);
        return body.json();
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });

