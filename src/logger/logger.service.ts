import { Injectable } from '@nestjs/common'
import * as winston from 'winston'

@Injectable()
export class LoggerService {
  private logger: winston.Logger

  constructor() {
    this.logger = winston.createLogger({
      transports: [new winston.transports.File({ filename: 'logs.txt' })]
    })
  }

  log(message: string) {
    this.logger.log('info', message)
  }
}
