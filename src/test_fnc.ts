export function testFncRange(value: any) {
    if (value < 0 || value > 5) {
        console.log('Вне диапазона');
        return false
    } else {
        console.log('Диапазон 0-5');
        return true
    }
}