import { Controller, Get, Param } from '@nestjs/common'
import { AppService } from './app.service'

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getStart(): string {
    return this.appService.getStart()
  }
  @Get('api')
  getSwagger(): string {
    return this.appService.getSwagger()
  }
}
