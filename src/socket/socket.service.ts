import { Injectable, OnModuleInit } from '@nestjs/common'
import { CreateSocketDto } from './dto/create-socket.dto'
import { LoggerService } from '../logger/logger.service'

@Injectable()
export class SocketService implements OnModuleInit {
  constructor(private readonly logger: LoggerService) {}

  onModuleInit() {
    // this.startLogging()
  }

  private startLogging() {
    setInterval(() => {
      this.newMessage({ message: 'Every 5 sec' })
    }, 5000)
  }

  newMessage(createSocketDto: CreateSocketDto) {
    let date = new Date()
    console.log('Send date:', date.toLocaleString(), 'Msg:', createSocketDto)
    this.logger.log(`Отправлено сообщение: ${createSocketDto.message}`)
    return createSocketDto
  }
}
