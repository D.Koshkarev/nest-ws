import { Module } from '@nestjs/common'
import { SocketService } from './socket.service'
import { SocketGateway } from './socket.gateway'
import { LoggerService } from '../logger/logger.service'

@Module({
  providers: [SocketGateway, SocketService, LoggerService]
})
export class SocketModule {}
