import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  WebSocketServer
} from '@nestjs/websockets'
import { SocketService } from './socket.service'
import { CreateSocketDto } from './dto/create-socket.dto'
import { Server } from 'socket.io'

@WebSocketGateway({
  cors: {
    origin: '*'
  }
})
@WebSocketGateway()
export class SocketGateway {
  constructor(private readonly socketService: SocketService) {}

  @WebSocketServer()
  server: Server

  @SubscribeMessage('message')
  handleMessage(@MessageBody() CreateSocketDto: CreateSocketDto): void {
    this.server.emit('message', this.socketService.newMessage(CreateSocketDto))
  }
}
