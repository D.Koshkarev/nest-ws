import { Module } from '@nestjs/common'
import { MdbProductService } from './mdb_product.service'
import { MdbProductController } from './mdb_product.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { MdbProduct, MdbProductSchema } from './schemas/mdb_product.schema'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MdbProduct.name, schema: MdbProductSchema }
    ])
  ],
  controllers: [MdbProductController],
  providers: [MdbProductService]
})
export class MdbProductModule {}
