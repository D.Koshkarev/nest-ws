import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { HydratedDocument } from 'mongoose'

export type MdbProductDocument = HydratedDocument<MdbProduct>

@Schema()
export class MdbProduct {
  @Prop()
  title: string

  @Prop()
  price: number
}

export const MdbProductSchema = SchemaFactory.createForClass(MdbProduct)
