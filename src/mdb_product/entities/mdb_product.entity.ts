import { ApiProperty } from '@nestjs/swagger'
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class MdbProductEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number | string

  @ApiProperty()
  @Column()
  title: string

  @ApiProperty()
  @Column()
  price: number
}
