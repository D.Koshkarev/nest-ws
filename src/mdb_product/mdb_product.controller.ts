import {
  Controller,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  Patch,
  Param,
  Delete
} from '@nestjs/common'
import { MdbProductService } from './mdb_product.service'
import { CreateMdbProductDto } from './dto/create-mdb_product.dto'
import { UpdateMdbProductDto } from './dto/update-mdb_product.dto'
import { MdbProduct } from './schemas/mdb_product.schema'
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger'

@ApiTags('mdb_product')
@Controller('api/mdb_product')
export class MdbProductController {
  constructor(private readonly mdbProductService: MdbProductService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Header('Content-Type', 'application/json')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create MdbProduct',
    type: MdbProduct
  })
  @ApiBody({ type: CreateMdbProductDto })
  @ApiQuery({ name: 'title', enum: ['PlayStation', 'Xbox'] })
  @ApiQuery({ name: 'price', type: Number, required: true })
  create(
    @Body() createMdbProductDto: CreateMdbProductDto
  ): Promise<MdbProduct> {
    return this.mdbProductService.create(createMdbProductDto)
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Get all MdbProduct',
    type: [MdbProduct]
  })
  findAll(): Promise<MdbProduct[]> {
    return this.mdbProductService.findAll()
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'Get one MdbProduct by id',
    type: MdbProduct
  })
  findOne(@Param('id') id: string): Promise<MdbProduct> {
    return this.mdbProductService.findOne(id)
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMdbProductDto: UpdateMdbProductDto
  ): Promise<MdbProduct> {
    return this.mdbProductService.update(id, updateMdbProductDto)
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  remove(@Param('id') id: string): Promise<MdbProduct> {
    return this.mdbProductService.remove(id)
  }
}
