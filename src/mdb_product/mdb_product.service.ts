import { Injectable } from '@nestjs/common'
import { CreateMdbProductDto } from './dto/create-mdb_product.dto'
import { UpdateMdbProductDto } from './dto/update-mdb_product.dto'
import { InjectModel } from '@nestjs/mongoose'
import { MdbProduct, MdbProductDocument } from './schemas/mdb_product.schema'
import { Model } from 'mongoose'

@Injectable()
export class MdbProductService {
  constructor(
    @InjectModel(MdbProduct.name)
    private mdbProductModel: Model<MdbProductDocument>
  ) {}

  async create(createMdbProductDto: CreateMdbProductDto): Promise<MdbProduct> {
    const newMbdProduct = new this.mdbProductModel(createMdbProductDto)
    return newMbdProduct.save()
  }

  async findAll(): Promise<MdbProduct[]> {
    return this.mdbProductModel.find().exec()
  }

  async findOne(id: string): Promise<MdbProduct> {
    return this.mdbProductModel.findById(id).exec()
  }

  update(
    id: string,
    updateMdbProductDto: UpdateMdbProductDto
  ): Promise<MdbProduct> {
    return this.mdbProductModel.findByIdAndUpdate(id, updateMdbProductDto, {
      new: true
    })
  }

  remove(id: string): Promise<MdbProduct> {
    return this.mdbProductModel.findByIdAndDelete(id)
  }
}
