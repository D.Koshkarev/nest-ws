import { ApiProperty } from '@nestjs/swagger'

export class CreateMdbProductDto {
  @ApiProperty({
    description: 'Name product'
  })
  title: string

  @ApiProperty({
    description: 'Price product',
    minimum: 1,
    required: true
  })
  price: number
}
