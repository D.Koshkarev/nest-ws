import { ApiProperty } from '@nestjs/swagger'
import { PartialType } from '@nestjs/mapped-types'
import { CreateMdbProductDto } from './create-mdb_product.dto'

export class UpdateMdbProductDto extends PartialType(CreateMdbProductDto) {
  @ApiProperty()
  readonly title: string

  @ApiProperty()
  readonly price: number
}
