import { ApiProperty } from '@nestjs/swagger'

export class MdbProductDto {
  @ApiProperty()
  readonly id: string | number

  @ApiProperty()
  readonly title: string

  @ApiProperty()
  readonly price: number
}
