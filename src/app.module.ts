import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { TestModule } from './test/test.module'
import { SocketModule } from './socket/socket.module'
import { MdbProductModule } from './mdb_product/mdb_product.module'
import { LoggerService } from './logger/logger.service'
import { TypeOrmModule } from '@nestjs/typeorm'

@Module({
  imports: [
    ConfigModule.forRoot({isGlobal: true}),
    TestModule,
    SocketModule,
    // mongoose DB
    // MongooseModule.forRoot('mongodb://0.0.0.0:27017'),
    // MongooseModule.forRootAsync({
    //     imports: [ConfigModule],
    //     inject: [ConfigService],
    //     useFactory: async (config: ConfigService) => ({
    //         uri: config.get<string>('MONGODB_URL')
    //       })
    //     }),
    // MdbProductModule,
        
    // postgress DB
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        type: 'postgres',
        host: config.get('DB_HOST'),
        port: config.get('DB_PORT'),
        username: config.get('DB_USERNAME'),
        password: config.get('DB_PASSWORD'),
        database: config.get('DB_NAME'),
        entities: [],
        synchronize: true,
        // entities: [__dirname + '/**/*.entity{.js, .ts}']
      })
    })
  ],
  controllers: [AppController],
  providers: [AppService, LoggerService]
})
export class AppModule {}
