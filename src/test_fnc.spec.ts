// import { testFncRange } from './test_fnc'

// test(
//     'Test testFncRange 0-5', () => {
//         expect(testFncRange(4)).toBe(true);
//     }, 0)
// test(
//     'Test testFncRange Вне диапазона', () => {
//         expect(testFncRange(6)).toBe(false);
//     }, 0)

import { testFncRange } from './test_fnc'

describe('testFncRange',
() => {
    const testCase = [
        {
            incomingValue: 4,
            expected: true
        },
        {
            incomingValue: 6,
            expected: false
        },
    ];
    testCase.forEach(item => {
        // it == test
        it(`Вводные данные: ${item.incomingValue}, ожидаю ${item.expected}`,
            () => {
                const res = testFncRange(item.incomingValue);
                expect(res).toBe(item.expected);
            }
        )
    })
});
