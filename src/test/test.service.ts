import { Injectable } from '@nestjs/common'
import { TestDto } from './dto/test.dto'
import { CreateTestDto } from './dto/create-test.dto'

@Injectable()
export class TestService {
  private testArr: TestDto[] = []

  getTestAll(): TestDto[] {
    return this.testArr
  }

  getTestById(id: number | string): TestDto {
    return this.testArr.find((i) => i.id === id)
  }

  createTest(createTestDto: CreateTestDto): string {
    this.testArr.push({
      ...createTestDto,
      id: Date.now().toString()
    })
    return 'Create success'
  }

  remove(id: number | string): string {
    this.testArr = this.testArr.filter((i) => i.id != id)
    return 'Remove success'
  }
}
