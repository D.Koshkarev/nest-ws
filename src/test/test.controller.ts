import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Req,
  Res
} from '@nestjs/common'
import { TestService } from './test.service'
import { TestDto } from './dto/test.dto'
import { CreateTestDto } from './dto/create-test.dto'
import { ApiTags } from '@nestjs/swagger'
// import { Response, Request } from 'express'

@ApiTags('test')
@Controller('api/test')
export class TestController {
  constructor(private readonly testService: TestService) {}

  // пример работы как в express
  // @Get('express')
  // getTestAllExpress(@Req() req: Request, @Res() res: Response): void {
  //   res.status(200).end('Response TestAll')
  // }

  @Get()
  getTestAll(): TestDto[] {
    return this.testService.getTestAll()
  }

  @Get(':id')
  getTestById(@Param('id') id: number | string): TestDto {
    return this.testService.getTestById(id)
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Header('Content-Type', 'application/json')
  create(@Body() createTestDto: CreateTestDto): string {
    return this.testService.createTest(createTestDto)
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  remove(@Param('id') id: number | string): string {
    return this.testService.remove(+id)
  }
}
