export class CreateTestDto {
  readonly id: string | number
  readonly title: string
  readonly price: number
}
