export class TestDto {
  readonly id: string | number
  readonly title: string
  readonly price: number
}
