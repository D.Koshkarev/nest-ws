# Разумеется нужны npm и node.js

# HELP https://docs.nestjs.com/first-steps

# Для windows через терминал среды разработки, PowerShell или cmd устанавливаем глобально
- npm i -g @nestjs/cli

# Если не распознает
- nest -v
- nest new project-name

# Проблема в среде окружения windows
# 1. Win + X и "Система".
# 2."Дополнительные параметры системы".
# 3. "Переменные среды".
# 4. В разделе "Системные переменные" -> Path -> "Изменить".
# 5. "Создать" и путь к папке с глобальными npm пакетами (обычно, C:\Users\Your User Name\AppData\Roaming\npm).
# 6. "ОК", и перегрузить командную строку

# Собрал примитивного клиента (client-ws) для теста

# Победил ошибку CORS в версии socket.io до 2 - она не возникает
- https://docs.nestjs.com/websockets/gateways

# Можно client-ws запустить в разных браузерах и увидеть магию

# Настроил CRUD
# Установил Swagger
# Подключил Mongo + настроил CORS

